import React from "react";
import LoginForm from "components/loginForm";
import { StyledFlex } from "ui/views/style";
import fakeApi from "fakeApi";
import { RouteComponentProps } from "react-router-dom";

const apiCall = (email: string, password: string) =>
  fakeApi.login(email, password);

export const LoginView: React.FC<RouteComponentProps> = (
  props: RouteComponentProps
) => (
  <StyledFlex>
    <LoginForm onSubmit={apiCall} {...props} />
  </StyledFlex>
);
