import React from "react";
import { StyledMainFlex } from "ui/layout";
import NavBar from "components/navbar";
import { LoginView } from "ui/views/LoginView";
import { SuccessView } from "ui/views/successView";
import { RouteComponentProps } from "react-router";
import { Wrapper } from "./style";
import Footer from "components/footer";

const Views: React.FC<RouteComponentProps> = props => {
  const renderView = () => {
    switch (props.match.path) {
      case "/loggedIn":
        return <SuccessView />;
      case "/home":
      case "/":
      default: {
        return <LoginView {...props} />;
      }
    }
  };

  return (
    <Wrapper>
      <NavBar />
      <StyledMainFlex>{renderView()}</StyledMainFlex>
      <Footer />
    </Wrapper>
  );
};

export default Views;
