import React from "react";
import { StyledFlex } from "ui/views/style";
import { H1 } from "components/globals";
import Emoji from "components/emoji";

export const SuccessView: React.FC = () => (
  <StyledFlex>
    <H1>
      OK!
      <Emoji symbol="👌" />
    </H1>
  </StyledFlex>
);
