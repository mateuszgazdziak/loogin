export default {
  bg: {
    default: "#FFFFFF",
    reverse: "#16171A",
    primary: "#10e0b7",
    secondary: "#000"
  },
  text: {
    default2: "#24292E",
    default: "#FFF",
    secondary: "#10e0b7",
    alt: "#67717A",
    placeholder: "#7C8894"
  }
};
