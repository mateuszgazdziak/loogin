import React from "react";
import { cleanup, render } from "@testing-library/react";

import { MainFlex, StyledMainFlex } from ".";

afterEach(cleanup);

describe("MainFlex component", () => {
  it("Should match snapshot", () => {
    const container = render(<MainFlex />);
    expect(container).toMatchSnapshot();
  });
});

describe("StyledMainFlex component", () => {
  it("Should match snapshot", () => {
    const container = render(<StyledMainFlex />);
    expect(container).toMatchSnapshot();
  });
});
