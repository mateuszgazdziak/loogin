import React from "react";
import { cleanup, render } from "@testing-library/react";

import { Wrapper, StyledFlex } from "./style";

afterEach(cleanup);

describe("Wrapper component", () => {
  it("Should match snapshot", () => {
    const container = render(<Wrapper />);
    expect(container).toMatchSnapshot();
  });
});

describe("StyledFlex component", () => {
  it("Should match snapshot", () => {
    const container = render(<StyledFlex />);
    expect(container).toMatchSnapshot();
  });
});
