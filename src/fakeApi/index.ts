class FakeApi {
  login(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      if (email === "test@test.pl" && password === "Password1") {
        return resolve({ status: 200 });
      } else {
        return reject({ status: 401, message: "wrong credentials" });
      }
    });
  }
}

export default new FakeApi();
