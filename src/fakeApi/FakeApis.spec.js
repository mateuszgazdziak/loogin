import fakeApi from ".";

describe("fakeApi", () => {
  it("Should pass test@test.pl/Password1", async () => {
    const result = await fakeApi.login("test@test.pl", "Password1");
    expect(result.status).toBe(200);
  });

  it("should rejct for non test@test.pl/Password1 credentials", async () => {
    try {
      await fakeApi.login("wrongtest@test.pl", "Password1");
    } catch (err) {
      expect(err.status).toBe(401);
    }
  });
});
