import React from "react";
import { Route, Switch, RouteComponentProps, withRouter } from "react-router";
import Views from "ui/views";

const Routes: React.FC<RouteComponentProps> = props => {
  return (
    <Switch>
      <Route exact path="/" component={Views} {...props} />
      <Route path="/loggedIn" component={Views} {...props} />
    </Switch>
  );
};

export default withRouter(Routes);
