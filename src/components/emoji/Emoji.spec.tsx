import React from "react";
import { cleanup, render } from "@testing-library/react";

import Emoji from ".";

afterEach(cleanup);

describe("Emoji component", () => {
  it("Should match snapshot", () => {
    const container = render(<Emoji symbol="👌" label="test" />);
    expect(container).toMatchSnapshot();
  });
});
