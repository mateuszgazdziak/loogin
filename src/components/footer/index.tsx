import React from "react";
import Emoji from "components/emoji";
import { StyledFooter } from "./style";

const Footer: React.FC = () => (
  <StyledFooter>
    <Emoji symbol="🐱‍🏍" /><Emoji symbol="🐱‍👤" /><Emoji symbol="🐱‍🚀" />
  </StyledFooter>
);

export default Footer;
