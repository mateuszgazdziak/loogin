import React from "react";
import { cleanup, render } from "@testing-library/react";

import Footer from ".";

afterEach(cleanup);

describe("Footer component", () => {
  it("Should match snapshot", () => {
    const container = render(<Footer />);
    expect(container).toMatchSnapshot();
  });
});
