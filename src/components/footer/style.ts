import styled from "styled-components";
import theme from "ui/theme";

export const StyledFooter = styled.div`
  background-color: ${theme.bg.reverse};
  color: ${theme.text.default};
  justify-content: center;
  display: flex;
  height: 64px;
  align-items: center;

  a {
    color: ${theme.text.default};
  }

  span {
    margin: 0 12px 0 12px;
    color: red;
  }
`;
