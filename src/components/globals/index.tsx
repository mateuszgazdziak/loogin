import styled, { css } from "styled-components";
import theme from "ui/theme";
import { MEDIA_BREAK } from "ui/layout";
export const fontStack = css`
  font-family: -apple-system, BlinkMacSystemFont, "Helvetica", "Segoe",
    sans-serif;
`;

export const FlexRow = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const FlexCol = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const H1 = styled.h1`
  color: ${theme.text.default};
  font-weight: 900;
  font-size: 1.5rem;
  line-height: 1.3;
  margin: 0;
  padding: 0;
`;

export const H2 = styled.h2`
  color: ${theme.text.default};
  font-weight: 900;
  font-size: 32px;
  line-height: 1.3;
  margin: 0;
  padding: 4px;
`;

export const H3 = styled.h3`
  color: ${theme.text.default};
  font-weight: 900;
  font-size: 24px;
  line-height: 1.3;
  margin: 0;
  padding: 0;
`;

export const SvgWrapper = styled.div`
  position: absolute;
  flex: none;
  z-index: 1;
  height: 80px;
  width: 110%;
  bottom: -4px;
  left: -5%;
  right: -5%;
  display: inline-block;
  pointer-events: none;

  @media (max-width: ${MEDIA_BREAK}px) {
    width: 150%;
    left: -25%;
    right: -25%;
  }
`;
