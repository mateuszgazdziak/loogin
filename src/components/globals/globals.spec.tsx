import React from "react";
import { render } from "@testing-library/react";
import {  FlexCol, FlexRow, H1, H2, H3 } from ".";

describe("H1 component", () => {
  test("Matches the snapshot", () => {
    const h1 = render(<H1 />);
    expect(h1).toMatchSnapshot();
  });
});

describe("H2 component", () => {
  test("Matches the snapshot", () => {
    const h2 = render(<H2 />);
    expect(h2).toMatchSnapshot();
  });
});

describe("H3 component", () => {
  test("Matches the snapshot", () => {
    const h3 = render(<H3 />);
    expect(h3).toMatchSnapshot();
  });
});

describe("FlexCol component", () => {
  test("Matches the snapshot", () => {
    const flexCol = render(<FlexCol />);
    expect(flexCol).toMatchSnapshot();
  });
});

describe("FlexRow component", () => {
  test("Matches the snapshot", () => {
    const flexRow = render(<FlexRow />);
    expect(flexRow).toMatchSnapshot();
  });
});
