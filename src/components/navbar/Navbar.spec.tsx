import React from "react";
import { cleanup, render } from "@testing-library/react";

import Navbar from ".";

afterEach(cleanup);

describe("Wrapper component", () => {
  it("Should match snapshot", () => {
    const container = render(<Navbar />);
    expect(container).toMatchSnapshot();
  });
});

describe("StyledFlex component", () => {
  it("Should match snapshot", () => {
    const container = render(<Navbar />);
    expect(container).toMatchSnapshot();
  });
});
