import React from "react";
import { cleanup, render } from "@testing-library/react";
import { Input, Button, InputContainer, ChckboxContainter } from "./style";

afterEach(cleanup);

describe("Input Form component", () => {
  it("Should match snapshot", () => {
    const container = render(<Input />);
    expect(container).toMatchSnapshot();
  });
});

describe("Button Form component", () => {
  it("Should match snapshot", () => {
    const container = render(<Button />);
    expect(container).toMatchSnapshot();
  });
});

describe("InputContainer Form component", () => {
  it("Should match snapshot", () => {
    const container = render(<InputContainer />);
    expect(container).toMatchSnapshot();
  });
});

describe("ChckboxContainter Form component", () => {
  it("Should match snapshot", () => {
    const container = render(<ChckboxContainter />);
    expect(container).toMatchSnapshot();
  });
});
