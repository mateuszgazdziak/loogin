import styled from "styled-components";
import theme from "ui/theme";

export const Input = styled.input`
  height: 40px;
  color: #fff;
  font-size: 25px;
  background: none;
  border: 1;
  border-radius: 5px;
  border-color: ${theme.text.secondary};
  align-self: flex-start;
  margin-bottom: 2px;

  &:focus {
    outline: none;
  }
`;

export const Button = styled.button`
  padding: 16px;
  margin: 2px;
  font-size: 16px;
  font-weight: 600;
  height: 64px;
  align-self: flex-end;
  color: ${theme.text.default};
  border-radius: 5px;
  background: none;
  cursor: pointer;
  border: none;
  background-color: ${theme.bg.reverse};

  &:hover {
    background: ${theme.bg.primary};
  }

  &:focus {
    box-shadow: 0 0 0 2px ${theme.bg.default}, 0 0 0 4px ${theme.bg.secondary};
    transition: box-shadow 0.2s ease-in-out;
  }
`;

export const InputContainer = styled.div`
  padding-bottom: 16px;
  display: flex;
  flex-direction: column;
`;

export const ChckboxContainter = styled.div`
  display: flex;
  flex-direction: row;
  text-transform: uppercase;
`;

export const Checkbox = styled.input`
  width: 16px;
  height: 16px;
`;
