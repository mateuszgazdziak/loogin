import { withFormik, FormikBag } from "formik";
import {
  LoginForm,
  IValues,
  IOuterProps
} from "components/loginForm/LoginForm";
import * as Yup from "yup";

export const ControlledLoginForm = withFormik<IOuterProps, IValues>({
  mapPropsToValues: (props: IOuterProps): IValues => ({
    email: "",
    password: ""
  }),
  validationSchema: Yup.object().shape({
    email: Yup.string()
      .email("Email is not valid")
      .required("Email is required"),
    password: Yup.string()
      .required("Password is required")
      .matches(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$/,
        "Not secure enough"
      )
      .min(6, "Password should have at least 6 characters")
  }),
  handleSubmit: (
    values: IValues,
    formikBag: FormikBag<IOuterProps, IValues>
  ) => {
    formikBag.props
      .onSubmit(values.email, values.password)
      .then(() => {
        formikBag.props.history.push("/loggedIn");
      })
      .catch(error => {
        alert(error.message);
        formikBag.resetForm();
      });
  }
})(LoginForm);
