import React from "react";
import { FormikProps } from "formik";
import { FlexCol } from "components/globals";
import { RouteComponentProps } from "react-router-dom";
import {
  Input,
  Button,
  ChckboxContainter,
  InputContainer,
  Checkbox
} from "./style";

export interface IValues {
  email: string;
  password: string;
}

export interface IOuterProps extends RouteComponentProps {
  onSubmit: (email: string, password: string) => Promise<any>;
}

export const LoginForm = ({
  errors,
  isSubmitting,
  touched,
  values,
  handleSubmit,
  handleBlur,
  handleChange
}: FormikProps<IValues> & IOuterProps) => {
  return (
    <form onSubmit={handleSubmit}>
      <FlexCol>
        <InputContainer>
          <Input
            id="login-form-email-input"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
            type="email"
            name="email"
            placeholder="email"
          />
          {errors.email && touched.email ? <span>{errors.email}</span> : null}
        </InputContainer>
        <InputContainer>
          <Input
            id="login-form-password-input"
            onChange={handleChange}
            onBlur={handleBlur}
            type="password"
            name="password"
            value={values.password}
            placeholder="password"
          />
          {errors.password && touched.password ? (
            <span>{errors.password}</span>
          ) : null}
        </InputContainer>
        <ChckboxContainter>
          Remember me:{" "}
          <Checkbox type="checkbox" name="remember" id="remember" />
        </ChckboxContainter>
        <Button
          role="submit"
          type="submit"
          disabled={
            isSubmitting ||
            !!(errors.email && touched.email) ||
            !!(errors.password && touched.password)
          }
        >
          Log In
        </Button>
      </FlexCol>
    </form>
  );
};
